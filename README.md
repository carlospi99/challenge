# challenge

## We need a new corporate website which sits separate from our usual production environments.
We want to give the marketing team a vanilla wordpress setup.
We want to ensure our site supports heavy loads should we ever be featured on a news website.
The database used will be MySql (assume any load balancing of the DB is out of scope).
Optionally we would like an SSL certificate, but don’t have budget to pay so can only allow a free service like Let’s Encrypt.

## Setting In Kubernetes

I have use Kubernetes to ensure tha the web can support heavey load and also can scale in case is needed automatically using CPU as metric for the scaling.

to use kubernetes locally you will need to have minikube install, docker desktop or rancher. IN my case I used rancher.

1. how to install https://docs.rancherdesktop.io/getting-started/installation/#linux
2. once is installed and running move to challenge-k8s folder and run
3. you will need also to installe kubectl https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/
``` 
kubectl apply -k ./
```
4. to see the wordpress locally run
```
kubectl port-forward deployment/wordpress 28017:80
```
5. on the browser do:
localhost:28017

this will show he wordpress ready to be configure

6. to stop portforwarding do control C
7. to remove the deployment ( will delete all data)
```
kubectl delete -k ./
```
if you want to keep the data just stop rancher and wordpress will come up when rancher is active again fromthe point it was
## settingnusing Docker

I have also configure using docker for also and easier deployment.
1. needs docker installed in the local machine
2. move to docker-wordpress folder
3. run:
```
sudo docker compose -f docker-compose-wordpress.yaml up -d
```
this will start up 4 container wordpress, mysql, nginx , certbot.
4. this can be access from browser pointing to http://localhost:80
5. you can stop the container 
```
sudo docker  stop webserver wordpress db
```
or remove them using 
```
sudo docker  rm webserver wordpress db
```
## issues.
I have to setup an laptop in ubuntu as I normally work in MAC but I don't do local deployments, I sue a dev environments. The MAC I sue is one of the M1 and not all docker images work on it.

to create the certificate with Let's Encrypt was a challenge, i used the certbot and it never create me the CA and the crt, it only issue the CSR, I tried to request it only the cert from my laptop but the dummy domain I used never passed the verification. I could do selfsign but wont be safe.. I will investigate why let's Encrypt failed.

```
sudo certbot certonly -d webpress-carlos.hopto.org --noninteractive --standalone --agree-tos --register-unsafely-without-email
```

I took longer than the two hour fixing the challenges I had with my tools.

If you need any clarification let me know..

Thank you

Carlos
